FROM python:3.7.10-slim

WORKDIR /root/

ENV FLASK_APP=src/serve_model.py
ENV FLASK_RUN_HOST=0.0.0.0

COPY . .

RUN python -m pip install --upgrade pip &&\
	pip install -r requirements.txt

RUN	python src/read_data.py &&\
	python src/text_preprocessing.py &&\
	python src/text_classification.py

CMD ["python", "src/serve_model.py"]