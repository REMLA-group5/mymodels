import json
import subprocess
import util


class TestMetrics(object):
    
    def test_accuracy_at_least_as_good(self):
        """
        The test accuracy should be at least 95% of the old test accuracy, as performance should not be significantly
        worse.
        """
        metrics_json = util.get_dvc_metrics()
        old = metrics_json['metrics/accuracy.json']['accuracy']['old']
        new = metrics_json['metrics/accuracy.json']['accuracy']['new']
        if old is None:
            assert new > 0.0 # pass if there is no old value
        else:
            assert old * 0.95 <= new

    def test_overfitting(self):
        """
        The test accuracy should not be lower than 90% of the train accuracy, as we would be overfitting otherwise.
        """
        metrics_json = util.get_dvc_metrics()
        train = metrics_json['metrics/accuracy_train.json']['accuracy']['new']
        test = metrics_json['metrics/accuracy.json']['accuracy']['new']
        assert train * 0.9 <= test

    def test_complexity(self):
        """
        When an increase in the model complexity is measured (given it is still the same model), that increase
        may not be higher than 5 times the increase in accuracy.
        """
        metrics_json = util.get_dvc_metrics()
        complexity = metrics_json['metrics/complexity.json']
        model_name = list(complexity.keys())[0]

        new_complexity = complexity[model_name]['new']
        old_complexity = complexity[model_name]['old']

        old_accuracy = metrics_json['metrics/accuracy.json']['accuracy']['old']
        new_accuracy = metrics_json['metrics/accuracy.json']['accuracy']['new']

        if old_complexity is None or old_accuracy is None or new_complexity is None or new_accuracy is None:
            assert True  # pass if there is no old value
        else:
            complexity_increase = new_complexity / old_complexity
            accuracy_increase = new_accuracy / old_accuracy
            assert complexity_increase < 5 * accuracy_increase

    def test_train_time(self):
        """
        When an increase in the model training time is measured (given it is still the same model), that increase
        may not be higher than 5 times the increase in accuracy.
        """
        metrics_json = util.get_dvc_metrics()
        train_time = metrics_json['metrics/train_time.json']
        model_name = list(train_time.keys())[0]
        new_train_time = train_time[model_name]['new']
        old_train_time = train_time[model_name]['old']

        old_accuracy = metrics_json['metrics/accuracy.json']['accuracy']['old']
        new_accuracy = metrics_json['metrics/accuracy.json']['accuracy']['new']

        if old_train_time is None or old_accuracy is None or new_train_time is None or new_accuracy is None:
            assert True  # pass if there is no old value
        else:
            train_time_increase = new_train_time / old_train_time
            accuracy_increase = new_accuracy / old_accuracy
            assert train_time_increase < 5 * accuracy_increase
