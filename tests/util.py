import subprocess
import json


def get_dvc_metrics():
    """
    Calculates the difference in metric scores between this branch and either the latest main commit, or the previous
    commit if we already are on the main branch.
    """
    branch_sp = subprocess.run('echo $CI_BUILD_REF_NAME', shell=True, stdout=subprocess.PIPE)
    branch = branch_sp.stdout.decode('utf-8').replace('\n', '')
    if branch == 'main':
        shout = subprocess.Popen('dvc metrics diff HEAD~1 --all --show-json', shell=True, stdout=subprocess.PIPE)
    else:
        shout = subprocess.Popen('dvc metrics diff main --all --show-json', shell=True, stdout=subprocess.PIPE)
    out = json.loads(shout.stdout.readlines()[0])
    return out