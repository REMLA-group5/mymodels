import configparser
import json
import subprocess


class TestModel(object):

    def test_perturbed_model_validation(self):
        """
        Ensure that the model being used is at least 80% as relevant as the optimal model.
        """
        config = configparser.ConfigParser()
        config.read("config.ini")
        model_name = config['DEFAULT']['ModelName']

        shout = subprocess.run('dvc metrics show --show-json', shell=True, stdout=subprocess.PIPE)
        out = json.loads(shout.stdout)
        scores = out['']['metrics/pmv.json']['PMV Score']

        assert scores[model_name] > 0.75 * max(scores.values())
