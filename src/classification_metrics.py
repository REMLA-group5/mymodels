import matplotlib.pyplot as plt
import pandas as pd
import json
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import seaborn as sn


def misclassified_messages(y_test, pred, test_messages, model_name):
    """
    Writes the misclassified messages to a text file.
    """
    file = open('output/misclassified_msgs.txt', 'a', encoding='utf-8')

    # write misclassified messages into a new text file
    file.write('\n#################### ' + model_name + ' ####################\n')
    file.write('\nMisclassified Spam:\n\n')
    for msg in test_messages[y_test < pred[model_name]]:
        file.write(msg)
        file.write('\n')
    file.write('\nMisclassified Ham:\n\n')
    for msg in test_messages[y_test > pred[model_name]]:
        file.write(msg)
        file.write('\n')
    file.close()


def accuracy_train(pred_scores_train, model_name):
    """
    Writes the train set accuracies of all models to disk.
    """
    print('\n############### Accuracy Scores Train ###############')
    accuracy = pd.DataFrame.from_dict(pred_scores_train, orient='index', columns=['Accuracy Rate'])
    print('\n')
    print(accuracy)
    print('\n')

    with open('metrics/accuracy_train.json', 'w') as f:
        json.dump({'accuracy': pred_scores_train[model_name]}, f)


def accuracy_test(pred_scores_test, model_name):
    """
    Writes the test set accuracies for all models to disk.
    """
    print('\n############### Accuracy Scores Test ###############')
    accuracy = pd.DataFrame.from_dict(pred_scores_test, orient='index', columns=['Accuracy Rate'])
    print('\n')
    print(accuracy)
    print('\n')

    with open('metrics/accuracy.json', 'w') as f:
        json.dump({'accuracy': pred_scores_test[model_name]}, f)


def complexity_score(classifier, model_name):
    """
    Writes the model complexity score to disk.
    """
    complexities = dict()
    complexities[model_name] = classifier.getModelComplexity()
    print('\n############### Complexity Scores ###############')
    complexity = pd.DataFrame.from_dict(complexities, orient='index', columns=['Complexity Score'])
    print('\n')
    print(complexity)
    print('\n')

    with open('metrics/complexity.json', 'w') as f:
        complexity.to_json(f)


def model_training_time(train_time, model_name):
    """
    Writes the model training time to disk.
    """
    print('\n############### Model Training Time (seconds) ###############')
    train_time_df = pd.DataFrame.from_dict(train_time, orient='index', columns=['Training Time'])
    print('\n')
    print(train_time_df)
    print('\n')

    with open('metrics/train_time.json', 'w') as f:
        train_time_df.to_json(f)


def generate_confusion_matrix(y_true, y_pred, labels):
    """
    Generates a confusion matrix of the results for one model.
    """
    cm = confusion_matrix(y_true, y_pred)
    df = pd.DataFrame(cm, index=labels, columns=labels)
    plt.figure(figsize=(10, 7))
    plt.title("Confusion Matrix")
    cm_plot = sn.heatmap(df, annot=True)
    cm_plot.figure.savefig('metrics/confusion_matrix.svg')


def generate_accuracy_plot(pred_scores):
    """
    Shows the accuracies of all models in a bar plot.
    """
    accuracy = pd.DataFrame.from_dict(pred_scores, orient='index', columns=['Accuracy Rate'])
    accuracy.plot(kind='bar', ylim=(0.85, 1.0), edgecolor='black', figsize=(10, 5))
    plt.ylabel('Accuracy Score')
    plt.title('Distribution by Classifier')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.savefig("output/accuracy_scores.png")