from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import (AdaBoostClassifier, BaggingClassifier,
                              RandomForestClassifier)

class Classifier():
    def __init__(self):
        pass

    def fit(self, X, y):
        self.classifier.fit(X, y)

    def predict(self, X):
        return self.classifier.predict(X)


class MultinomialNBClassifier(Classifier):
    def __init__(self):
        super().__init__()
        self.classifier = MultinomialNB()

    def getModelComplexity(self):
        [n_classes, n_features] = self.classifier.feature_count_.shape
        return n_classes + n_features

class EnsembleClassifier(Classifier):
    def __init__(self, type):
        super().__init__()
        if type == 'AdaBoost':
            self.classifier =  AdaBoostClassifier()
        elif type == 'Bagging Classifier':
            self.classifier = BaggingClassifier()
        elif type == 'Random Forest':
            self.classifier = RandomForestClassifier()
        else:
            self.classifier = None
    
    def getModelComplexity(self):
        return len(self.classifier.estimators_)

class KNNClassifier(Classifier):
    def __init__(self):
        super().__init__()
        self.classifier = KNeighborsClassifier()

    def getModelComplexity(self):
        return self.classifier.n_samples_fit_

class SVMClassifier(Classifier):
    def __init__(self):
        super().__init__()
        self.classifier = SVC()
    
    def getModelComplexity(self): 
        return self.classifier.n_support_.shape[0]

class DTClassifier(Classifier):
    def __init__(self):
        super().__init__()
        self.classifier = DecisionTreeClassifier()

    def getModelComplexity(self): 
        return self.classifier.get_depth() ** 2 + self.classifier.get_n_leaves()
