import configparser

import numpy as np
import pandas as pd
from joblib import load
from sklearn.metrics import accuracy_score

from text_classification import my_train_test_split, model_selection, train_classifier, predict_labels
from text_preprocessing import _load_data


def flip_label(series):
    """
    Flips labels around as noise in the data.
    """
    for i, v in series.items():
        if v == 'ham':
            series[i] = 'spam'
        elif v == 'spam':
            series[i] = 'ham'
    return series


def perturb_data(training_data):
    """
    Generates 5 noisy datasets with 10, 20, 30, 40 and 50% noise.
    """
    res = {}
    for i in range(10, 60, 10):
        perturbed = flip_label(training_data.sample(frac=(i * 0.01), random_state=1))
        copy = training_data.copy()
        copy.update(perturbed)
        res[i] = copy
    return res


def calculate_PMV(accuracies):
    """
    calculates the Perturbed Model Validation score.
    See https://hal.inria.fr/hal-02139208v2/document
    """
    keys = np.array(list(accuracies.keys()))
    keys = keys * 0.01
    values = np.array(list(accuracies.values()))
    return np.abs(np.polyfit(keys, values, 1)[0])


def validate_model(model):
    """
    Validates the model using PMV
    """
    raw_data = _load_data()
    preprocessed_data = load('output/preprocessed_data.joblib')

    classifier = model_selection(model)

    (X_train, X_test, y_train, y_test,) = my_train_test_split(preprocessed_data, raw_data['label'])

    perturbed_data_dict = perturb_data(y_train)

    accuracies = {}
    for r, perturbed_data in perturbed_data_dict.items():
        print(f"Fitting {model} with {r}% noise...")
        train_classifier(classifier, X_train, perturbed_data)
        accuracies[r] = accuracy_score(y_test, predict_labels(classifier, X_test))

    pmv = calculate_PMV(accuracies)
    return pmv


def find_optimal_model(models, current):
    """
    Finds the optimal model to use.
    """

    pmv_scores = {}

    for model in models:
        pmv = validate_model(model)
        pmv_scores[model] = pmv
        print(f"PMV score for {model} is {pmv}.")

    if pmv_scores[current] < max(pmv_scores.values()):
        optimal_model = max(pmv_scores, key=pmv_scores.get)
        print(f"Using suboptimal {current} with PMV of {pmv_scores[current]}, "
              f"{optimal_model} with PMV of {pmv_scores[optimal_model]} is a better fit for the data.")
    else:
        print("Using optimal model.")

    return pmv_scores


def save_metrics(metric):
    """
    Saves the PMV scores in a metric file for dvc.
    """
    scores = pd.DataFrame.from_dict(metric, orient='index', columns=['PMV Score'])
    with open('metrics/pmv.json', 'w') as f:
        scores.to_json(f)


if __name__ == "__main__":
    # Read config file
    config = configparser.ConfigParser()
    config.read("config.ini")
    model_name = config['DEFAULT']['ModelName']
    all_models = config['DEFAULT']['Models'].split(', ')

    save_metrics(find_optimal_model(all_models, model_name))

