"""
Train a model on the SMS Spam Collection data set using different algorithms.
"""

import configparser
from models import DTClassifier, EnsembleClassifier, KNNClassifier, MultinomialNBClassifier, SVMClassifier

import time
from joblib import dump, load
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

from classification_metrics import *
from text_preprocessing import _load_data

import json

# matplotlib.use('TkAgg')
pd.set_option('display.max_colwidth', None)
# Read config file
config = configparser.ConfigParser()
config.read("config.ini")


def my_train_test_split(*datasets):
    """
    Split dataset into training and test sets. We use a 70/30 split.
    """
    return train_test_split(*datasets, test_size=0.3, random_state=101)


def train_classifier(classifier, X_train, y_train):
    """
    Fits the classifier on the samples in X_train with labels y_train.
    """
    classifier.fit(X_train, y_train)


def predict_labels(classifier, X_test):
    """
    Returns the predictions for the samples in X_test given by the classifier.
    """
    return classifier.predict(X_test)


def model_selection(name):
    """
    Sets the classifier used in the pipeline to the one given.
    """
    classifier = None
    if name == 'SVM':
        classifier = SVMClassifier()
    elif name == 'Decision Tree':
        classifier = DTClassifier()
    elif name == 'Multinomial NB':
        classifier = MultinomialNBClassifier()
    elif name == 'KNN':
        classifier = KNNClassifier()
    elif name == 'Random Forest':
        classifier = EnsembleClassifier('Random Forest')
    elif name == 'AdaBoost':
        classifier = EnsembleClassifier('AdaBoost'),
    elif name == 'Bagging Classifier':
        classifier = EnsembleClassifier('Bagging Classifier')
    else:
        print("ERROR: invalid model name specified in config")
        # FIXME: do we want a generic model?
        exit(1)
    return classifier


def save_metrics(model_name, classifier, duration, pred_scores_train, pred_scores_test):
    """
    Calls all metric methods.
    """
    with open('metrics/model_name.json', 'w') as f:
        json.dump({'Model Name': model_name}, f)

    complexities = dict()
    train_time = dict()

    complexities[model_name] = classifier.getModelComplexity()
    train_time[model_name] = duration

    accuracy_train(pred_scores_train, model_name)
    accuracy_test(pred_scores_test, model_name)
    complexity_score(classifier, model_name)
    model_training_time(train_time, model_name)



def save_plots(labels, y_test, pred_test, pred_scores_test, model_name):
    """
    Calls all plot generation methods.
    """
    generate_accuracy_plot(pred_scores_test)
    generate_confusion_matrix(y_test, pred_test[model_name], labels)


def main():
    classifier = model_selection(model_name)

    raw_data = _load_data()
    preprocessed_data = load('output/preprocessed_data.joblib')

    (X_train, X_test,
     y_train, y_test,
     _, test_messages) = my_train_test_split(preprocessed_data,
                                             raw_data['label'],
                                             raw_data['message'])

    start = time.time()
    train_classifier(classifier, X_train, y_train)
    duration = time.time() - start

    pred_scores_train = dict()
    pred_train = dict()
    pred_scores_test = dict()
    pred_test = dict()

    # predict training set
    pred_train[model_name] = predict_labels(classifier, X_train)
    pred_scores_train[model_name] = accuracy_score(y_train, pred_train[model_name])
    print('\n############### ' + model_name + ' Train ###############\n')
    print(classification_report(y_train, pred_train[model_name]))

    # predict test set
    pred_test[model_name] = predict_labels(classifier, X_test)
    pred_scores_test[model_name] = accuracy_score(y_test, pred_test[model_name])
    print('\n############### ' + model_name + ' Test ###############\n')
    print(classification_report(y_test, pred_test[model_name]))

    misclassified_messages(y_test, pred_test, test_messages, model_name)
    save_metrics(model_name, classifier, duration, pred_scores_train, pred_scores_test)
    save_plots(['ham', 'spam'], y_test, pred_test, pred_scores_test, model_name)

    # Store "best" classifier

    dump(classifier, 'output/model.joblib')


if __name__ == "__main__":
    model_name = config['DEFAULT']['ModelName']
    main()
