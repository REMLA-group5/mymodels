"""
Flask API of the SMS Spam detection model model.
"""
#import traceback
import joblib
from flask import Flask, jsonify, request, render_template
from flasgger import Swagger
import pandas as pd

from text_preprocessing import prepare, _extract_message_len, _text_process
import configparser
from dotenv import load_dotenv, dotenv_values
import markdown.extensions.fenced_code

app = Flask(__name__)
swagger = Swagger(app)

# Read config file
config = configparser.ConfigParser()
config.read("config.ini")
env = dotenv_values(".env")

@app.route('/')
def my_form():
    try:
        report = open('report.md', "r")
        metrics = markdown.markdown(report.read(), extensions=['tables'])
    except:
        metrics = None

    return render_template('index.html', version=version, metrics=metrics)

@app.route('/', methods=['POST'])
def my_form_post():
    try:
        report = open('report.md', "r")
        metrics = markdown.markdown(report.read(), extensions=['tables'])
    except:
        metrics = None

    sms = request.form['text']

    processed_sms = prepare(sms)
    model = joblib.load('output/model.joblib')
    prediction = model.predict(processed_sms)[0]

    res = {
        "result": prediction,
        "classifier": model_name,
        "sms": sms
    }
    
    return render_template('index.html', version=version, result=res, metrics=metrics)


@app.route('/predict', methods=['POST'])
def predict():
    """
    Predict whether an SMS is Spam.
    ---
    consumes:
      - application/json
    parameters:
        - name: input_data
          in: body
          description: message to be classified.
          required: True
          schema:
            type: object
            required: sms
            properties:
                sms:
                    type: string
                    example: This is an example of an SMS.
    responses:
      200:
        description: "The result of the classification: 'spam' or 'ham'."
    """
    input_data = request.get_json()
    sms = input_data.get('sms')
    processed_sms = prepare(sms)
    model = joblib.load('output/model.joblib')
    prediction = model.predict(processed_sms)[0]

    res = {
        "result": prediction,
        "classifier": "decision tree",
        "sms": sms
    }
    print(res)
    return jsonify(res)

@app.route('/dumbpredict', methods=['POST'])
def dumb_predict():
    """
    Predict whether a given SMS is Spam or Ham (dumb model: always predicts 'ham').
    ---
    consumes:
      - application/json
    parameters:
        - name: input_data
          in: body
          description: message to be classified.
          required: True
          schema:
            type: object
            required: sms
            properties:
                sms:
                    type: string
                    example: This is an example of an SMS.
    responses:
      200:
        description: "The result of the classification: 'spam' or 'ham'."
    """
    input_data = request.get_json()
    sms = input_data.get('sms')

    return jsonify({
        "result": "Spam",
        "classifier": "decision tree",
        "sms": sms
    })

if __name__ == '__main__':
    model_name = config['DEFAULT']['ModelName']
    port = env['PORT']
    version = open("src/resources/version.txt", "r").readline()
    app.run(host="0.0.0.0", port=port, debug=True)
