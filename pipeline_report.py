import json
import subprocess
import pandas as pd


def pipeline_report():
    """
    Generates a pipeline report using the metrics in the metrics/ folder.
    """
    report = "This report has been automatically generated.  \n"
    try:
        model_name = json.decoder.JSONDecoder().decode(open("metrics/model_name.json").readline())['Model Name']
        accuracy_train = json.decoder.JSONDecoder().decode(open("metrics/accuracy_train.json").readline())['accuracy']
        accuracy_test = json.decoder.JSONDecoder().decode(open("metrics/accuracy.json").readline())['accuracy']
        complexity_scores = json.decoder.JSONDecoder().decode(
            open("metrics/complexity.json").readline())['Complexity Score']
        training_times = json.decoder.JSONDecoder().decode(open("metrics/train_time.json").readline())['Training Time']

        report += "Currently active model: {}  \n".format(model_name)
        report += "- Train Accuracy:\t\t{:0.3f}  \n".format(accuracy_train)
        report += "- Test Accuracy: \t\t{:0.3f}  \n".format(accuracy_test)
        report += "- Complexity Score: \t{}  \n".format(complexity_scores[model_name])
        report += "- Training Time: \t\t{:0.3f}s  \n".format(training_times[model_name])

    except Exception as e:
        report += "Error while parsing metrics."
    return report


def pretty_print_dvc_metrics_diff():
    """
    Generates a pipeline report using `dvc metrics diff`. A comparison is made to the latest main commit, or the
    previous commit if we already are on the main branch.
    """
    report = "This report has been automatically generated.  \n   \n"

    try:
        model_name = json.decoder.JSONDecoder().decode(open("metrics/model_name.json").readline())['Model Name']
        old_model_name = get_old_model_name()
    except Exception as e:
        report += "An error was encountered while trying to read `model_name.json`!  \n"
        report += str(e)
        return report

    try:
        j = get_dvc_metrics()

        df = pd.DataFrame(columns=['Metric', 'Old', 'New'])
        df.loc[0] = ['Model Name', old_model_name, model_name]
        df.loc[1] = ['Train Accuracy', j['metrics/accuracy_train.json']['accuracy']['old'],
                     j['metrics/accuracy_train.json']['accuracy']['new']]
        df.loc[2] = ['Test Accuracy', j['metrics/accuracy.json']['accuracy']['old'],
                      j['metrics/accuracy.json']['accuracy']['new']]
        df.loc[3] = ['Complexity', j['metrics/complexity.json']['Complexity Score.{}'.format(old_model_name)]['old'],
                     j['metrics/complexity.json']['Complexity Score.{}'.format(model_name)]['new']]
        df.loc[4] = ['Training Time', j['metrics/train_time.json']['Training Time.{}'.format(old_model_name)]['old'],
                     j['metrics/train_time.json']['Training Time.{}'.format(model_name)]['new']]
        df.loc[5] = ['PMV', j['metrics/pmv.json']['PMV Score.{}'.format(old_model_name)]['old'],
                     j['metrics/pmv.json']['PMV Score.{}'.format(model_name)]['new']]
        report += df.to_markdown(index=False) + "  \n  \n"
    except Exception as e:
        report += str("An error was encountered while trying to read metrics!  \n")
        report += str(e)
    return report


def get_dvc_metrics():
    """
    Calculates the difference in metric scores between this branch and either the latest main commit, or the previous
    commit if we already are on the main branch.
    """
    branch_sp = subprocess.run('echo $CI_BUILD_REF_NAME', shell=True, stdout=subprocess.PIPE)
    branch = branch_sp.stdout.decode('utf-8').replace('\n', '')
    if branch == 'main':
        shout = subprocess.Popen('dvc metrics diff HEAD~1 --all --show-json', shell=True, stdout=subprocess.PIPE)
    else:
        shout = subprocess.Popen('dvc metrics diff main --all --show-json', shell=True, stdout=subprocess.PIPE)
    out = json.loads(shout.stdout.readlines()[0])
    return out


def get_old_model_name():
    """
    Determines the model name of the model used in either the latest main commit, or the previous commit if we already
    are on the main branch.
    """
    branch_sp = subprocess.run('echo $CI_BUILD_REF_NAME', shell=True, stdout=subprocess.PIPE)
    branch = branch_sp.stdout.decode('utf-8').replace('\n', '')
    if branch == 'main':
        shout = subprocess.Popen('git show HEAD~1:metrics/model_name.json', shell=True, stdout=subprocess.PIPE)
    else:
        shout = subprocess.Popen('git show origin/main:metrics/model_name.json', shell=True, stdout=subprocess.PIPE)
    out = json.loads(shout.stdout.readlines()[0])
    old_model_name = out['Model Name']
    return old_model_name


print(pretty_print_dvc_metrics_diff())
