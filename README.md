# How It's Made: Reliable and Efficient Machine Learning Pipelines

### REMLA Group 5

- Toon de Boer
- Thomas Bos
- Daniel van Gelder
- Ricardo Jongerius

The codebase was originally adapted from: https://github.com/rohan8594/SMS-Spam-Detection


# Web API

The web API can either be run by the latest release or cloning this repository.

## Release

### Latest Release

a) Run latest release.
```
docker pull registry.gitlab.com/remla-group5/mymodels
docker run --rm -p 3000:3000 registry.gitlab.com/remla-group5/mymodels
```

b) Test latest release on http://localhost:3000

### Any other Release

a) Run any release with a version tag.
```
docker pull registry.gitlab.com/remla-group5/mymodels:[tag]
docker run --rm -p 3000:3000 registry.gitlab.com/remla-group5/mymodels:[tag]
```
example:
```
docker pull registry.gitlab.com/remla-group5/mymodels:0.7-latest
docker run --rm -p 3000:3000 registry.gitlab.com/remla-group5/mymodels:0.7-latest
```

b) Test on http://localhost:3000

## Clone repository and run latest development

a) Clone the repo.
```
git clone https://gitlab.com/REMLA-group5/mymodels.git
cd mymodels
```

b) Install the requirements, pull data from DVC and run the project.
```
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
pip install 'dvc[gdrive]'
mkdir output
dvc pull
dvc exp run -f
```

c) Run tests (optional).
```
pytest
```

d) Run local docker-compose.
```
docker-compose up
```

e) Test local development on http://localhost:3000


# Run Gitlab CI locally for MacOS

a) Install gitlab-runner.

```
brew install gitlab-runner
```

b) Register runner.

```
gitlab-runner register
```

And set up runner according to the instructions on gitlab: https://gitlab.com/REMLA-group5/mymodels/-/settings/ci_cd

c) Run the pipeline with `[name]` being the job to execute

```
gitlab-runner exec shell [name]
```
